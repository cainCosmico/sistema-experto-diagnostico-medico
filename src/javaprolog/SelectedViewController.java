/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaprolog;

import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Estudiante
 */
public class SelectedViewController  extends AnchorPane {
    @FXML private ListView<String> sourceList;
    @FXML private ListView<String> targetList;
    
    public SelectedViewController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "FXMLListSelectedView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
     
    public ObservableList<String> getTargetItems() {
        return targetList.getItems();
    }
    
    public ObservableList<String> getSourceItems() {
        return this.sourceList.getItems();
    }
    
    @FXML void onClickAddToTarget(ActionEvent event) {
        String aux = this.sourceList.getSelectionModel().getSelectedItem();
        if (this.sourceList.getItems().size() == 0) return;
        this.targetList.getItems().add(aux);
        this.sourceList.getItems().remove(aux);
    }
    
    @FXML void onClickAddToSource(ActionEvent event) {
        String aux = this.targetList.getSelectionModel().getSelectedItem();
        if (this.targetList.getItems().size() == 0) return;
        this.sourceList.getItems().add(aux);
        this.targetList.getItems().remove(aux);
    }
    
}
