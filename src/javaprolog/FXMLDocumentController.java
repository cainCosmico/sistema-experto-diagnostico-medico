/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaprolog;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import org.jpl7.Query;
import org.jpl7.Term;

/**
 *
 * @author Estudiante
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML private ChoiceBox<String> box;
    @FXML private AnchorPane content_paneListView;
    @FXML private ListView<String> listEnfermedades;
    @FXML private TextArea textTratamiento;
    private SelectedViewController listSelected;
    
    @FXML private ImageView cabeza;
    @FXML private ImageView pulmones;
    @FXML private ImageView corazon;
    @FXML private ImageView abdomen; 
    
    @FXML
    public void OnBtnCabeza(MouseEvent event) {
        reset(cabeza);      
        getSintomas(getEnfermedades("cabeza"));
    }
    
    public void OnBtnAbdomen(MouseEvent event) {
        reset(abdomen); 
        getSintomas(getEnfermedades("abdominal"));
    }
    
    public void OnBtnPulmones(MouseEvent event) {
        reset(pulmones); 
        getSintomas(getEnfermedades("respiratoria"));
    }
    
    public void OnBtnCorazon(MouseEvent event) {
        reset(corazon); 
        getSintomas(getEnfermedades("corazon"));
    }
    
    
    @FXML void OnBtnDiagnostico(ActionEvent event) {
        listEnfermedades.getItems().clear();
        System.out.println("source: " + this.listSelected.getSourceItems().size());
        System.out.println("target: " + this.listSelected.getTargetItems().size());
        
        listEnfermedades
                .getItems()
                .addAll(getEnfermedadPorSintoma
        (observableListToLinkedList(this.listSelected.getTargetItems())));
    }
    
    @FXML void OnBtnLimpiar () {
        listEnfermedades.getItems().clear();
        clearSelectedView();
    }
    
    public void reset(ImageView img){
        cabeza.getStyleClass().add("parte_cuerpo");
        pulmones.getStyleClass().add("parte_cuerpo");
        corazon.getStyleClass().add("parte_cuerpo");
        abdomen.getStyleClass().add("parte_cuerpo");
        img.getStyleClass().clear();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initWidgets();
        conectarProlog();
    }   
    
    public LinkedList<String> observableListToLinkedList(ObservableList<String> obList) {
        LinkedList<String> newList = new LinkedList<>();
        for (String string : obList) {
            newList.add(string);
        }
        
        return newList;
    }
    
    public void initWidgets() {        
        listSelected = new SelectedViewController();
        this.content_paneListView.getChildren().add(listSelected);
        
        listEnfermedades.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                //System.out.println("Click listener: " + oldValue + " new: " + newValue);
                getTratamiento((String)newValue);
            }
        });     
    }
    
    public void conectarProlog() {
        String conection = "consult('src/javaprolog/enfermedades.pl')";//aqui colocan el nombre de su archivo a compilar;
        Query query = new Query(conection);
        System.out.println(conection + " " + (query.hasSolution() ? "exito" : "fallo")); //mostrara mensaje  si hay o no conexion
    }
    
    public void getTratamiento(String enfermedad) {
        textTratamiento.clear();
        String text = "tiene_propiedad(" + enfermedad + " ,tratamiento,L)";
        Query q3 = new Query(text);  
        Map<String, Term>[] resultList = q3.allSolutions();

        LinkedList<String> list = this.getItemsInList(resultList);
        for (String string : list) {
            textTratamiento.appendText("->" + string + "\n");
        }
    }
    
    public void getSintomas(LinkedList<String> enfermedades) {
        int i = 0;
        this.clearSelectedView();
        
        for (String enfermedad : enfermedades) {
            String text = "tiene_propiedad(" + enfermedad + " ,sintoma,L)";
            //System.out.println(i + "sintomas: " + text);
            Query q3 = new Query(text);  
            Map<String, Term>[] resultList = q3.allSolutions();

            LinkedList<String> list = this.getItemsInList(resultList);

            listSelected.getSourceItems().addAll(list);
            i++;
        }
    }
    
    public LinkedList<String> getEnfermedadPorSintoma(LinkedList<String> sintomas) {
        LinkedList<String> list = new LinkedList<>();
        for (int i=0; i<sintomas.size(); i++) {
            String text = "enfermedad(" + sintomas.get(i) + ",L)";
            System.out.println("sintomas: " + text);
            Query q4 = new Query(text);  
            Map<String, Term>[] resultList = q4.allSolutions();

            LinkedList<String> auxList = this.getItemsInList(resultList); // <- 
            System.out.println("auxlist: " + auxList.toString());
            
            for (int j=0; j<auxList.size(); j++) {
                if (list.size() > 0) {
                    for (int k=0; k<list.size(); k++) {
                        System.out.println("i: " + list.get(k) + " j: " + auxList.get(j));
                        if (auxList.get(j) == list.get(k))  continue;
                        else list.add(auxList.get(j));                            
                    }
                } else {
                    list.add(auxList.get(j));
                }
            }
            
        }
        
        for (String string : list) {
            System.out.println("enfermedad por sintoma: " + string);
        }
        
        //no_repetir(list);
        
        return no_repetir(list);   
    }

    /**
     * Retorna las enfermedades correspondientes a una parte dada
     * @param parte {"cabeza","corazon"}
     * @return list
     */
    public LinkedList<String> getEnfermedades(String parte) {
        // modificar consulta para obtener linkedlist deseados
        String text = "tiene_propiedad(" + parte + " ,enfermedad,L)";
        Query q2 = new Query(text);  
        Map<String, Term>[] resultList = q2.allSolutions();

        LinkedList<String> list = this.getItemsInList(resultList);
        //Codigo añadido la puta madre
        
        for (int i = 0; i < no_repetir(list).size(); i++) {
            System.out.println(" ...... " + no_repetir(list).get(i));
        }
        
        this.box.getItems().addAll(list);
        
        return list;
    }   
    
    private void clearSelectedView() {
        this.listSelected.getSourceItems().clear();
        this.listSelected.getTargetItems().clear();
    }
    
    /**
     * 
     * @param resultSet Map<String, Term>[]
     * @return <list> con la list tipo prolog
     */
    public LinkedList<String> getItemsInList(Map<String, Term>[] resultSet) {
        LinkedList<String> list = new LinkedList<>();
        for (int i=0; i<resultSet.length; i++) {
            Iterator it = resultSet[i].keySet().iterator();
            while(it.hasNext()){
                String key = (String) it.next();
                //System.out.println(i + " Clave: " + key + " -> Valor: " + resultSet[i].get(key));
                String[] s =  resultSet[i].get(key)
                        .toString().replaceAll("[^\\\\a-z,_]", "")
                        .split(",");
                
                for (String str : s) {
                    //System.out.println("str: " + str); 
                    //selectedView.getSourceItems().add(str);
                    //this.box.getItems().add(str); // <- Aqui inserta la respuesta a combo box;
                    list.add(str);
                }
            }
        }
        
        return list;
    }
    
    // ESTE CODIGO DE AQUI SIRVE COMO MAQUETA PARA PROGRAMAR LAS FUNCIONES
    /*
    public void runProlog() {
        String t1 = "consult('src/javaprolog/test.pl')";//aqui colocan el nombre de su archivo a compilar
        Query q1 = new Query(t1);
        System.out.println(t1 + " " + (q1.hasSolution() ? "exito" : "fallo")); //mostrara mensaje  si hay o no conexion
        
        
        String t2 = "tiene_propiedad(X,Y,L)";
        Query q2 = new Query(t2);  
        Map<String, Term>[] map = q2.allSolutions();
        
        for (int i=0; i<map.length; i++) {
            // 1.
            int j = 0;
            for (Term key : map[i].values()) {
                System.out.println("i: " + i + " j: " + j + " key: " + key);
                j++;
            }
            // 2.
//            Stream.of(map[i].values().toString()).forEach(System.out::println);
            
            // 3.
//            Iterator<String> itr = map[i].keySet().iterator();
//            while(itr.hasNext()) {
//                System.out.println("map[0]" + itr.next());
//            }
        }
        

        
        //System.out.println(q2.allSolutions().toString() ) ;  
    }
    */
    
    public LinkedList<String> no_repetir(LinkedList<String> lista){
        LinkedList<String> local = new LinkedList<>();
        
        local.add(lista.get(0));
        
        for (int i = 0; i < lista.size(); i++) {
            if (!enLista(local, lista.get(i))) {
                local.add(lista.get(i));
            }
        }
        
        return local;
    }
    
    public boolean enLista(LinkedList<String> lista, String elemento){
        for (int i = 0; i < lista.size(); i++) {
            if (elemento.compareTo(lista.get(i))==0) {
                System.out.println("está en lista");
                return true;
            }
        }
        return false;
    }
    
}
