% enfermedad
tiene_propiedad(enfermedad,tiene,[abdominal,corazon,respiratoria,cabeza]).

% abdominal
tiene_propiedad(abdominal,enfermedad,[intoxicacion,apendicitis,gastritis]).
tiene_propiedad(intoxicacion,sintoma,[fiebre,vomito,diarrea,debilidad_muscular,sed_excesiva,resequedad_en_la_boca]).
tiene_propiedad(intoxicacion,tratamiento,[mantener_una_hidratacion_adecuada,tomar_suero]).
tiene_propiedad(apendicitis,sintoma,[fiebre,vomito,dolor_zona_baja_derecha,nauseas,diarrea,estrenimiento]).
tiene_propiedad(apendicitis,tratamiento,[visite_a_su_medico,extraccion_del_apendice]).
tiene_propiedad(gastritis,sintoma,[dolor_en_la_zona_superior_del_abdomen,reflujo,aceides_estomacal,sensacion_de_saciedad_en_la_parte_superior_del_abdomen]).
tiene_propiedad(gastritis,tratamiento,[tomar_antiacidos,tomar_omeprazol,tomar_amoxicilina]).

% corazon
tiene_propiedad(corazon,enfermedad,[angina,arritmia,valvulopatia]).
tiene_propiedad(corazon,sintoma,[ritmo_cardiago_irregular,dolor_de_pecho,dificultad_para_respirar]).
tiene_propiedad(angina,sintoma,[dolor_de_brazos,dolor_de_cuello,dolor_de_mandibula,dolor_de_espalda,nauseas,fatigas,sudoracion,mareos]).
tiene_propiedad(angina,tratamiento,[tomar_aspirina,tomar,carvedilol]).
tiene_propiedad(arritmia,sintoma,[demsayo,mareo,aturdimiento,sencacion_de_aleteo]).
tiene_propiedad(arritmia,tratamiento,[tomar_flecainida,implantar_marcapasos]).
tiene_propiedad(valvulopatia,sintoma,[cansancio,tobillos_hinchados,demsayo]).
tiene_propiedad(valvulopatia,tratamiento,[visite_a_su_medico]).

% cabeza
tiene_propiedad(cabeza,enfermedad,[epilepsia,migrania]).
tiene_propiedad(epilepsia,tratamiento,[tomar_medicamento_anticonvulsivo_o_antiepileptico,visite_a_su_medico]).
tiene_propiedad(epilepsia,sintoma,[convulsiones,perdida_del_conocimiento,espasmos_icontrolables,episodios_de_ausencia,confusion_temporal]).
tiene_propiedad(migrania,tratamiento,[tomar_aspirina,tomar_ibuprofeno,tomar_sumatriptan]).
tiene_propiedad(migrania,sintoma,[vision_borrosa,sensibilidad_a_los_sonidos,sensibilidad_a_la_luz,dolor_pulsante,sensacion_de_vertigo]).

% respiratoria
tiene_propiedad(respiratoria,enfermedad,[gripe,rinitis,epoc,sinusitis]).
tiene_propiedad(respiratoria,sintoma,[dolor_de_cabeza]).
tiene_propiedad(gripe,sintoma,[dolor_de_garganta,tos_seca,malestar_general,escalofrios,fiebre,conegstion_nasal]).
tiene_propiedad(gripe,tratamiento,[ibuprofeno,amoxicilina,descanso,ingerir_liquidos]).
tiene_propiedad(rinitis,sintoma,[ojos_llorosos,tos,irratibilidad,estornudos,picazon_en_la_garganta]).
tiene_propiedad(rinitis,tratamiento,[tomar_bromfeniramina,evitar_areas_con_mucho_polvo]).
tiene_propiedad(epoc,sintoma,[falta_de_energia,color_azul_en_los_labios,dificultad_para_respirar_durante_actividades_fisicas,silvido_al_respirar,presion_en_el_pecho,exceso_de_mucosidad_en_los_pulmones_en_horas_de_la_maniana,tos_cronica_con_flema_verdosa_clara]).
tiene_propiedad(epoc,tratamiento,[usar_inhalador_de_rescate,usar_esteroides_antiinflamatorios]).
tiene_propiedad(sinusitis,sintoma,[fiebre,malestar_general,fatiga,perdida_del_olfato,presin_detras_de_los_ojos,dolor_detras_de_los_ojos,conegstion_nasal,secrecion_nasal]).
tiene_propiedad(sinusitis,tratamiento,[lavado_nasal_con_solucion_salina,tomar_ibuprofeno]).

% reglas ojala funque
enfermedad(Sintoma,E):-tiene_propiedad(E,sintoma,List),member(Sintoma,List).

