% Clases 
subclase(vehiculo,vehiculo_automotriz).

% Propiedades
tiene_propiedad(vehiculo_automotriz,tiene,[motor,sistemas_de_frenos,sistema_de_combustion,sistema_electrico,llantas,transmision]).
/*--------------MOTOR---------------*/
tiene_propiedad(motor,tiene,[carter,culatas,bloque_motor]).
tiene_propiedad(carter,tiene,[ciguenial,cojinetes,volante_de_motor]).
tiene_propiedad(culatas,tiene,[bujias,camara_de_combustion,valvulas,guias_y_acientos,arbol_de_levas]).
tiene_propiedad(bloque_motor,tiene,[junta_culata,cilindros,pistones,anillos,bulones,biclas]).

% Falla 
/*CARTER*/
tiene_propiedad(ciguenial,falla,[rayanada_muniequillas,rotura_por_fatiga]).
tiene_propiedad(cojinetes,falla,[lubricacion,maltrato]).
tiene_propiedad(volante_de_motor,falla,[embrague,desgaste_de_componenetes]).

/*CULATAS*/
tiene_propiedad(bujias,falla,[deposito_de_aceite,deposito_de_carbon,residuos_de_impurezas]).
tiene_propiedad(camara_de_combustion,falla,[partes_con_oxido,rajaduras]).
tiene_propiedad(valvulas,falla,[montaje_y_ajuste,mecanizacion_defectuosa,montaje_de_piezas_desgastadas]).
tiene_propiedad(guias_y_acientos,falla,[montaje_de_piezas_desgastadas]).
tiene_propiedad(arbol_de_levas,falla,[leva_rota,gripaje_apoyo_del_eje]).

/*BLOQUE MOTOR*/
tiene_propiedad(junta_culata,falla,[sobrecalentamiento_motor,perdida_liquido_refrigerante]).
tiene_propiedad(cilindros,falla,[falla_motor]).
tiene_propiedad(pistones,falla,[sobrecalentamiento_motor,sistema_de_refrijeracion]).
tiene_propiedad(anillos,falla,[fabrica,error_de_montura]).
tiene_propiedad(bulones,falla,[error_de_montura,sobrecarga_a_tension]).
tiene_propiedad(biclas,falla,[demaciada_revolucion,fatiga,falla_del_pasador]).

% Solucion
tiene_propiedad(rayanada_muniequillas,solucion,[llevar_al_mecanico]).
tiene_propiedad(rotura_por_fatiga,solucion,[llevar_al_mecanico]).
tiene_propiedad(lubricacion,solucion,[llevar_al_mecanico]).
tiene_propiedad(maltrato,solucion,[llevar_al_mecanico]).
tiene_propiedad(embrague,solucion,[llevar_al_mecanico]).
tiene_propiedad(desgaste_de_componenetes,solucion,[llevar_al_mecanico]).

/*CULATAS*/
tiene_propiedad(deposito_de_aceite,solucion,[rectificar_cilindros,cambiar_relacion_aceite_vs_combustible]).
tiene_propiedad(deposito_de_carbon,solucion,[bujia_mas_caliente,sustituir_bujia,cambiar_marca_combustible,comprobar_fuga_aceite]).
tiene_propiedad(residuos_de_impurezas,solucion,[sustituir_bujia,cambiar_marca_combustible,comprobar_fuga_aceite]).
tiene_propiedad(partes_con_oxido,solucion,[llevar_al_mecanico]).
tiene_propiedad(rajaduras,solucion,[llevar_al_mecanico]).
tiene_propiedad(montaje_y_ajuste,solucion,[llevar_al_mecanico]).
tiene_propiedad(mecanizacion_defectuosa,solucion,[llevar_al_mecanico]).
tiene_propiedad(montaje_de_piezas_desgastadas,solucion,[llevar_al_mecanico]).
tiene_propiedad(gripaje_apoyo_del_eje,solucion,[comprobar_apoyo,llevar_al_mecanico]).
tiene_propiedad(leva_rota,solucion,[comprobar_apoyo,llevar_al_mecanico]).

/*BLOQUE MOTOR*/
tiene_propiedad(sobrecalentamiento_motor,solucion,[llevar_al_mecanico,reposicion_liquido]).
tiene_propiedad(perdida_liquido_refrigerante,solucion,[llevar_al_mecanico,reposicion_liquido]).
tiene_propiedad(falla_motor,solucion,[llevar_al_mecanico]).
tiene_propiedad(sobrecalentamiento_motor,solucion,[llevar_al_mecanico]).
tiene_propiedad(sistema_de_refrijeracion,solucion,[llevar_al_mecanico]).
tiene_propiedad(fabrica,solucion,[llevar_al_mecanico]).
tiene_propiedad(error_de_montura,solucion,[llevar_al_mecanico]).
tiene_propiedad(sobrecarga_a_tension,solucion,[llevar_al_mecanico]).
tiene_propiedad(demaciada_revolucion,solucion,[llevar_al_mecanico]).
tiene_propiedad(fatiga,solucion,[llevar_al_mecanico]).
tiene_propiedad(falla_del_pasador,solucion,[llevar_al_mecanico]).

/*--------------SISTEMA DE FRENOS---------------*/
tiene_propiedad(sistemas_de_frenos,tiene,[freno_de_emergencia,tambores_de_freno,servo_freno,cilindro_principal,pedal]).

/*falla*/
tiene_propiedad(freno_de_emergencia,falla,[no_enciende_luz,no_sirve]).
tiene_propiedad(tambores_de_freno,falla,[tambores_agrietados,marcas_termicas]).
tiene_propiedad(servo_freno,falla,[demaciado_tiempo_en_frenar,pedal_duro_permanentemente]).
tiene_propiedad(cilindro_principal,falla,[liquido_de_freno_contaminado]).
tiene_propiedad(pedal,falla,[excesiva_carrera_pedal,pedal_demaciado_duro]).

/*solucion*/
tiene_propiedad(no_enciende_luz,solucion,[reemplazar_bombillo]).
tiene_propiedad(no_sirve,solucion,[llevar_al_mecanico]).
tiene_propiedad(marcas_termicas,solucion,[cambiar_tambor]).
tiene_propiedad(tambores_agrietados,solucion,[cambiar_tambor]).
tiene_propiedad(demaciado_tiempo_en_frenar,solucion,[llevar_al_mecanico]).
tiene_propiedad(pedal_duro_permanentemente,solucion,[llevar_al_mecanico,limpiar_filtro]).
tiene_propiedad(liquido_de_freno_contaminado,solucion,[verificar_liquido_de_freno,llevar_al_mecanico]).
tiene_propiedad(excesiva_carrera_pedal,solucion,[verificar_pastillas]).
tiene_propiedad(pedal_demaciado_duro,solucion,[verificar_liquido]).

/*--------------SISTEMA DE COMBUSTION---------------*/
tiene_propiedad(sistema_de_combustion,tiene,[carburadores_de_combustibles]).

/*falla*/
tiene_propiedad(carburadores_de_combustibles,falla,[base_acoplamiento_suelta,juntas_carburador_mal_estado]).

/*solucion*/
tiene_propiedad(base_acoplamiento_suelta,solucion,[localizar_filtro_de_aire_y_reemplazar]).
tiene_propiedad(juntas_carburador_mal_estado,solucion,[ajustar_tornillos_carburador]).

/*--------------SISTEMA ELECTRICO---------------*/
tiene_propiedad(sistema_electrico,tiene,[sistema_de_generacion_y_almacenamiento,sistema_de_encendido,motor_de_arranque,sistema_de_inyeccion_de_gasolina,sistema_de_iluminacion]).

/*falla*/
tiene_propiedad(sistema_de_generacion_y_almacenamiento,falla,[carga_escasa_o_nula,carga_excesiva]).
tiene_propiedad(sistema_de_encendido,falla,[bateria_descargada,switch_malo,contactos_quemados,bobina_primaria_desconectada]).
tiene_propiedad(motor_de_arranque,falla,[bateria_descargada,conexiones_flojas]).
tiene_propiedad(sistema_de_inyeccion_de_gasolina,falla,[fugas_de_combustible,potencia_motor_abrupta]).
tiene_propiedad(sistema_de_iluminacion,falla,[bombilla_fundida,mala_instalacion,bateria_descargada]).

/*solucion*/
tiene_propiedad(carga_escasa_o_nula,solucion,[revisar_correa]).
tiene_propiedad(carga_excesiva,solucion,[revisar_alternador,cambiar_regulador]).
tiene_propiedad(bateria_descargada,solucion,[cargar_reemplazar_bateria]).
tiene_propiedad(switch_malo,solucion,[reemplazar_switch]).
tiene_propiedad(contactos_quemados,solucion,[reemplazar_contactos]).
tiene_propiedad(bobina_primaria_desconectada,solucion,[conectar_bobina]).
tiene_propiedad(conexiones_flojas,solucion,[comprobar_conexion]).
tiene_propiedad(fugas_de_combustible,solucion,[cambiar_inyectores]).
tiene_propiedad(potencia_motor_abrupta,solucion,[cambiar_inyectores]).
tiene_propiedad(mala_instalacion,solucion,[llevar_al_mecanico]).
tiene_propiedad(bombilla_fundida,solucion,[reemplazar_bombillo]).

/*--------------LLANTAS---------------*/
/*fallas*/
tiene_propiedad(llantas,falla,[presion_baja,machucones,impacto_costado,cortes]).

/*solucion*/
tiene_propiedad(presion_baja,solucion,[aumentar_presion]).
tiene_propiedad(machucones,solucion,[cambiar_llanta]).
tiene_propiedad(impacto_costado,solucion,[cambiar_llanta]).
tiene_propiedad(cortes,solucion,[cambiar_llanta]).

/*--------------transmision---------------*/
tiene_propiedad(transmision,tiene,[palieres,diferencial,arbol_de_transmision,caja_de_cambios]).

/*fallas*/
tiene_propiedad(palieres,fallas,[holgura_en_transmisiones]).
tiene_propiedad(diferencial,fallas,[ruido_en_curvas,sireneo_al_acelerar]).
tiene_propiedad(arbol_de_transmision,fallas,[ruidos]).
tiene_propiedad(caja_de_cambios,fallas,[no_entran_marchas,ruidos_caja,bloqueo_al_cambiar_de_marcha]).

/*solucion*/
tiene_propiedad(holgura_en_transmisiones,solucion,[llevar_al_mecanico]).
tiene_propiedad(ruido_en_curvas,solucion,[llevar_al_mecanico]).
tiene_propiedad(sireneo_al_acelerar,solucion,[llevar_al_mecanico]).
tiene_propiedad(ruidos,solucion,[llevar_al_mecanico]).
tiene_propiedad(no_entran_marchas,solucion,[tensar_el_embrague]).
tiene_propiedad(ruidos_caja,solucion,[tensar_el_embrague]).
tiene_propiedad(bloqueo_al_cambiar_de_marcha,solucion,[nuevos_bolillos_de_seguridad]).


/*===================================REGLAS===========================*/

% 1. Listas los partes y sus subcategorias
%% componentes(X,L):-
%%     tiene_propiedad(X,tiene,L).

% 2. Fallas de componentes: Lista los componentes y sus posibles fallas
%% fallas(X,L):-
%%     tiene_propiedad(X,falla,L).

% 3. Solucinoes: Listas la falla y su solucion
%% soluciones(C,F,S):-
%%     tiene_propiedad(C,falla,List),
%%     member(F,List),
%%     tiene_propiedad(F,solucion,S),nl.


/*
 * Realizado por: 
 * Ricardo Bastidas Luna
 * Camilo Valencia Romo
 * Sistemas Expertos 
 * Parcial #2
 * Redes semanticas
*/


